import Stream._

sealed abstract class Stream[+A] {
  def uncons: Option[Cons[A]]
  def isEmpty: Boolean = uncons.isEmpty


  def toList: List[A] = {
    if (uncons.isEmpty) List[A]() else uncons.get.head :: uncons.get.tail.toList
  }

  def toList2: List[A] = uncons match {
    case Some(x) => x.head::x.tail.toList2
    case _=>List()
  }

  def take(k:Int):Stream[A]= uncons match{
    case Some(x) if (k>=1) => cons(x.head, x.tail.take(k-1))
    case _=> empty
  }

  def takeWhile(p: A => Boolean): Stream[A] = uncons match {
    case Some(x) if p(x.head)=> cons(x.head, x.tail.takeWhile(p))
    case Some(x) => x.tail.takeWhile(p)
    case _=>empty
  }

  def foldRight[B](z: => B)(f: (A, => B) => B): B =
    uncons match {
      case Some(c) => f(c.head, c.tail.foldRight(z)(f))
      case None => z
    }

  def exists(p: A => Boolean): Boolean =
    foldRight(false)((a, b) => p(a) || b)


  def takeWhile_foldRight(p: A => Boolean): Stream[A] =
    foldRight(Stream.empty[A])((a, b) => if (p(a)) Stream.cons(a, b) else b)



  def map[B](p: => A => B): Stream[B] =
    foldRight(Stream.empty[B])((a,b) => Stream.cons(p(a), b))

  def filter(p: => A => Boolean): Stream[A] =
    foldRight(Stream.empty[A])((a,b) => if(p(a)) Stream.cons(a, b); else b)

  def append[B>:A](s: Stream[B]): Stream[B] =
    foldRight(s)((a,b) => Stream.cons(a, b))



  def add[B>:A](b: => B):Stream[B] =
    append(cons(b, empty[B]))

  def flatMap[B>:A](p: => A => Stream[B]): Stream[B] =
    foldRight(Stream.empty[B])((a,b) => b.append(p(a)))

}



object Empty extends Stream[Nothing] {
  val uncons = None
}


sealed abstract class Cons[+A] extends Stream[A] {
  def head: A
  def tail: Stream[A]
  val uncons = Some(this)
}

object Stream {
  def empty[A]: Stream[A] = Empty
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = new Cons[A] {
    val head = hd
    val tail = tl
  }

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) Empty else cons(as.head, apply(as.tail: _*))

}

object ScalaApp{
  def main(args: Array[String]): Unit = {
    val v = Stream(1,2,3,4)
    val w = v.takeWhile_foldRight(_> 3)
    println(w.toList2)

  }
}