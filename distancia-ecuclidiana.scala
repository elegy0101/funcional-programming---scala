def calculoDistancia(xuno:Int,yuno:Int,xdos:Int,ydos:Int, i:Int)(personaDistancia:(Int, Int, Int, Int, Int)=>String): String ={
  val resultado = personaDistancia(i, xuno,yuno, xdos,ydos)
  s"La distancia $resultado"
}



calculoDistancia(8, 9, 7,10, 1) (personaDistancia = (i:Int,xuno:Int,yuno:Int,xdos:Int,ydos:Int) => i match {
  case 1 => "Manhathan es " + (Math.abs(xdos - xuno) + Math.abs(ydos - yuno)).toString;
  case 2 => "Euclidiana es " + Math.sqrt(Math.pow(xdos - xuno,2).toInt + Math.pow(ydos - yuno,2).toInt).toString
})

calculoDistancia(8, 9, 7,0, 2) (personaDistancia = (i:Int,xuno:Int,yuno:Int,xdos:Int,ydos:Int) => i match {
  case 1 => "Manhathan es " + (Math.abs(xdos - xuno) + Math.abs(ydos - yuno)).toString;
  case 2 => "Euclidiana es " + Math.sqrt(Math.pow(xdos - xuno,2).toInt + Math.pow(ydos - yuno,2).toInt).toString
})
