/** 5. Repeat the preceding exercise with a java.util.Treemap that
*   you adapt to the Scala API.
*/

val count = new java.util.TreeMap[String, Int]().asScala.withDefaultValue(0)
val in = new java.util.Scanner(new java.io.File("/Users/nephtaliceballosflores/Documents/file.txt"))
while (in.hasNext()){
  val words = in.next()
  count += words -> (count.getOrElse(words, 0) +1 )

}
