/** 6. Define a linked hash map that maps "Monday" to java.util.-
*   Calendar.Monday, and similary for the other weekdays.
*   Demostrate that the elements are visited in insertion order.
*/

val semana = scala.collection.mutable.Map[String, Int]()

semana += ("Lunes" -> java.util.Calendar.MONDAY)
semana += ("Martes" -> java.util.Calendar.TUESDAY)
semana += ("Miercoles" -> java.util.Calendar.WEDNESDAY)
semana += ("Jueves" -> java.util.Calendar.THURSDAY)
semana += ("Viernes" -> java.util.Calendar.FRIDAY)
semana += ("Sabado" -> java.util.Calendar.SATURDAY)
semana += ("Domingo" -> java.util.Calendar.SUNDAY)

