/** 8. Write a function minmax(values: Array[Int]) that returns a pair
*   containing the smallest and largest values in the array.
*/

def minmax(values: Array[Int]): (Int, Int) = {
    (values.min,values.max)
}

println(minmax(Array(2,4,8,10)))