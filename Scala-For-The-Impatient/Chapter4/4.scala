/** 4. Repeat the preceding exercise with a sorted map, so that
*   the words are printed in sorted order.
*/

var count = scala.collection.immutable.SortedMap[String, Int]()
val in = new java.util.Scanner(new java.io.File("/Users/nephtaliceballosflores/Documents/file.txt"))
while (in.hasNext()){
  val words = in.next()
  count += words -> (count.getOrElse(words, 0) +1 )

}
