/** 2. Write a program that reads words from a file. Use a mutable map to count
* how often each word apper. To read the words simply use a java.util.scanner:
* val in = new java.util.Scanner(new java.io.File("myfile.txt")) 
* while (in.hasNext()) process in.next()
*/


val count = scala.collection.mutable.Map[String, Int]()
val in = new java.util.Scanner(new java.io.File("/Users/nephtaliceballosflores/Documents/file.txt"))
while (in.hasNext()) {
  val words = in.next()
  count(words) = count.getOrElse(words, 0) + 1
}

