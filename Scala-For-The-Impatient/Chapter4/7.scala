/** 7. Print a table for all java propierties, like this
*   java.runtime.name     | Java(TM) SE Runtime Environment 
*   sun.boot.library.path | /home/apps/jdk1.6.0_21/jre/lib/i386 
*   java.vm.version       | 17.0-b16
*   java.vm.vendor        | Sun Microsystems Inc.
*   java.vendor.url       | http://java.sun.com/ 
*   path.separator        | : 
*   java.vm.name          | Java HotSpot(TM) Server VM
*    
*   You need to find the length of the longest key before
*   you can print the table.
*/

val props: scala.collection.Map[String, String] = {
  System.getProperties() }.asScala

val maxLength = (for ((key, _) <- props) yield key.length).max

for ((key, value) <- props)
  printf("%-" + maxLength + "s | %s\n", key, value)