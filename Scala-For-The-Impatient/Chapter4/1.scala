/** 1. Set up a map of prices for a number of gizmos that you covet. 
*      Then produce a second map with the same keys and the prices at a 10 percent discount.
*/

val gizmos = Map("Macbook Pro 2019" -> 35000, "Epiphone Les Paul" -> 12000, "Tesla Model 3" -> 700000)

val gizmosDiscount = for((gizmo, price) <- gizmos) yield (gizmo, price * 0.9)
