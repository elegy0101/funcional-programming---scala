/**
* 7. Solve the preceding exercise without writing a loop.
* (Hint: Look at the StringOps Scaladoc
**/

def productoSinLoop(s: String): Int = {
   s.map(_.toInt).product
 }
