/**
* 6. Write a for loop for computing the product of the Unicode codes
* of all letters in a string.
* For example, the product of the characters in "Hello" is 825152896.
**/

def productoLoop(s: String): Int = {
    var resultado = 1
    for (c <- s) resultado *= c.toInt

    resultado
  }
