// 9.  Make the function of the preceding exercise a recursive function.


def productoRecursivo(s: String): Int = {
  if (s.tail != "") s.head.toInt * productoRecursivo(s.tail)
  else s.head.toInt
}
