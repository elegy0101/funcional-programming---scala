/**
* Write a function that computes xn, where n is an integer. Use the following recursive definition:”
* {{{
* x^n = y^2 if n is even and positive, where y = x^(n / 2).
* x^n = x * x^(n – 1) if n is odd and positive.
* x^0 = 1.
* x^n = 1 / (x^–n) if n is negative.
* }}}
* Don't use a 'return' statement.
**/

def compute(x: Double, n: Int): Double = {
  if (n == 0) 1
  else {
    if (n > 0) {
      if ( n % 2 == 0 && n > 2) {
        compute(compute(x, n/2), 2)
      } else {
        x * compute(x, n - 1)
      }
    } else (1 / compute(x, -n))
  }
}
