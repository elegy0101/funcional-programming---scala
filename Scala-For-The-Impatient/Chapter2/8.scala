/**
* 8. Write a function product(s : String) that computes the product,
* as described in the preceding exercises.
**/

def producto(s: String): Int ={
  s.map(_.toInt).product
}
