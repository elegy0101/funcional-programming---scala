## ¿Qué espero aprender con la materia de Programación Funcional?

*  Nephtali Ceballos Flores

*  Maestría en Ingeniería de Software

*  CIMAT Zacatecas


La programación funcional ha tenido un auge importante en los últimos años. La demanda de software cada vez más complejo y el incremento exponencial de usuarios que utilizan servicios basados en la nube han puesto en la mira a la programación funcional como un enfoque para resolver problemas que los paradigmas actuales no logran satisfacer. 

Con base en lo anterior, considero la importancia de aprender sobre este nuevo paradigma con el objetivo principal de conocer el estado actual y ver hacia dónde se dirige, de manera que a través de la práctica pueda identificar cuándo utilizarla y cómo puede ayudar a construir aplicaciones de nueva generación.

Mi tema de tesis es acerca de la Implementación de Metodologías Ágiles Basadas en Lean a través de Juegos Serios, no tiene una relación directa con la programación funcional. Sin embargo, al igual que los juegos serios representan una alternativa como método de enseñanza, espero tratar el tema de la programación funcional como un nuevo enfoque a tomar en cuenta para futuros desarrollos de software.

## Libro Mastering Functional Programming - Questions

## Higher-order functions 

1. How are functions interpreted in the context of object-oriented programming?

   **R:** En el contexto de POO representan el comportamiento de los objetos.

2. How are functions interpreted in the context of pure functional programming?

   **R:** En el contexto de la programación funcional pura se representan como cálculos matématicos.

3. What are higher-order functions?

   **R:** Son funciones que pueden recibir una función como argumento.

4. Why are higher-order functions useful?

   **R:** Aumentan considerablemente la capacidad de abstracción, creando estructuras de control que permiten crear
   aplicaciones que pueden ser expresadas siguiendo el principio DRY (Don't Repeat Yourself).

## Functional Data Structures

1. What is the general approach you take when writing an imperative collections-based application?
    
     **R:** Se trabaja con determinado tipo de abstracción sobre la multiplicidad.

2. What is the general approach you take when writing a functional collections-based application?

     **R:** Las colecciones de datos y su abstracción son inmutables. Esto quiere decir que cada vez que se crea un
            objeto ya no puede ser modificado.
            
3. Why is it not necessary to be trained in algorithm reasoning when dealing with functional data structures (in the majority of cases)?

     **R:** Las colecciones de datos y su transformación son tratadas como expresiones algebraicas que calculan un 
            valor basado en su entrada.

4. What is the algebraic approach to programming?

     **R:** Se refiere a que la programación se representa como expresiones matemáticas, donde existen dos elementos principales:
            operadores y operandos. Los operandos pueden entenderse como datos, la información que se desea manipular, mientras
            que los operadores se refieren al comportamiento, como se van a utilizar los datos.

5. What are the benefits of adopting an algebraic style?

     **R:** Uno de los beneficios es la noción de ausencia de cambio de manera que a través de expresiones algebraicas no existe
           dimensión de tiempo o secuencia asociada. Simplemente se escribe una expresión algebraica y el lenguaje le asigna 
           una semántica. Además permite separar la semántica de la estrucura de un programa.

6. What is the purpose of effect types such as Option or Try?

     **R:** Remover los efectos secundarios de los programas.
     
